let calculate = require('../functions.js')
let assert = require('assert');

describe("#1: Multiples of 3 and 5", function(){
	it("should give 23 as sum of multiples of 3 or 5 below 10", function(){
		assert.equal(calculate.calculateSumEvenFibonacciNumbers(4000000), 23);
	});
});


describe("#1: Multiples of 3 and 5", function(){
	it("should give 23 as sum of multiples of 3 or 5 below 10", function(){
		assert.equal(calculate.calculateMultiplesOf3Or5(10), 23);
	});
});