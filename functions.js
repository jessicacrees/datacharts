let generate = {};

/*==========================
	When the page loads:
	Run these functions
===========================*/
var callback = function () {
	/* #1: GDPR Compliance */
//	document.getElementById("chart-1-gdpr-compliance-donut-chart-labels").innerHTML = "";
	generate.buildGDPRComplianceDonutChartWithLabels(data1GDPRCompliance);
};

if (document.readyState === "complete" || (document.readyState !== "loading" && !document.documentElement.doScroll)) {
	callback();
} else {
	document.addEventListener("DOMContentLoaded", callback);
}





var data1GDPRCompliance = [
	{sector: "Deletion", actual: 48, outOf: 70},
	{sector: "Sharing", actual: 36, outOf: 40},
	{sector: "Protection", actual: 8, outOf: 20},
	{sector: "Offline", actual: 28, outOf: 65},
	{sector: "Disaster Recovery", actual: 15, outOf: 80}
];


generate.buildGDPRComplianceDonutChartWithLabels = function(dataset) {
	var donut = d3.pie()
				.value(function(dataset){
					return (dataset.actual/dataset.outOf)/1*100
				})
				.sort(null)
				.padAngle(.03);

	var w = 300,
		h = 300;

	var outerRadius = w/2;
	var innerRadius = 100;

	var colour = d3.scaleOrdinal(d3.schemeCategory10);

	var arc = d3.arc()
				.outerRadius(outerRadius)
				.innerRadius(innerRadius);

	var svg = d3.select("#chart-1-gdpr-compliance-donut-chart-labels")
				.append("svg");
	
	svg.attr({
		width: w,
		height: h,
		classed: "shadow"
	});
	
	
	svg.append("g")
		.attr({
			transform: "translate(" + w/2 + ", " + h/2 + ")"
		});

	var path = svg.selectAll("path")
				.data(donut(dataset))
				.enter()
				.append("path");
	
		path.attr({
			data: arc,
			fill: function(data, index){
				return colour(data.dataset.sector)
			}
		});
}











///* #2: Even Fibonacci Numbers */
//calculate.calculateSumEvenFibonacciNumbers = function(notExceed) {
//	
//	var count = 0;
//	
//	var sumOfEvenNumbers = 0;
//	
//	var fibonacciSequence = [1, 2];
//	
//	var currentNumber = 0;
//	
//	while(currentNumber <= notExceed){
//		
//		var previousNumber = fibonacciSequence[count];
//	
//		currentNumber = fibonacciSequence[count+1];
//	
//		var nextNumber = previousNumber + currentNumber;
//		
//		fibonacciSequence.push(nextNumber);
//		
//		if(currentNumber%2 == 0){
//			sumOfEvenNumbers += currentNumber;
//		}
//
//		count++;
//	}
//	
//	return sumOfEvenNumbers;
//}
//
//
///* #1: Multiples of 3 and 5 */
//calculate.calculateMultiplesOf3Or5 = function(toCalculate){
//	var multiplesOf3Or5 = [];
//	
//	var sumOfMultiples = 0;
//	
//	// Loop through numbers below the one given as a parameter
//	for(var i = toCalculate-1; i > 0; i--){
//		if(i%3 == 0 || i%5 == 0){
//			// Number is divisible by 3 or 5 so add to array
//			// Unnecessary for this project but why not have it
//			multiplesOf3Or5.push(i);
//			
//			// Also add to the sum total
//			sumOfMultiples += i;
//		}
//	}
//	
//	return sumOfMultiples;
//}


//module.exports = generate;